package it.insubria.cappello.usedbooks2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


enum class FragmentTypes{
    MAP, LIST
}

class MainActivity : AppCompatActivity() {

    private var pActiveFragment = FragmentTypes.MAP
    private lateinit var pEmail : String
    lateinit var pUserId: String
    var IsPersonalList = false
    var DocumentToShow : Document? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(intent.hasExtra("email"))
            pEmail = intent.getStringExtra("email")

        if(intent.hasExtra("userId"))
            pUserId = intent.getStringExtra("userId")



        ChangeFragment.setOnClickListener{

            this.IsPersonalList = false
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                when{
                    pActiveFragment == FragmentTypes.MAP -> {
                        replace<ListFragment>(R.id.fragment_container)
                        pActiveFragment = FragmentTypes.LIST
                        ChangeFragment.setImageResource(R.drawable.ic_map)
                    }
                    pActiveFragment == FragmentTypes.LIST -> {
                        replace<MapFragment>(R.id.fragment_container)
                        pActiveFragment = FragmentTypes.MAP
                        ChangeFragment.setImageResource(R.drawable.ic_list)
                    }
                    else -> {
                        Toast.makeText(this@MainActivity, "Temporary Error", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        change_info.setOnClickListener {
            val intent =Intent(this@MainActivity, RegisterActivity::class.java)
            intent.putExtra("userId", pUserId)
            startActivity(intent)
        }

        addDocument.setOnClickListener {

            val intent =Intent(this@MainActivity, CreateDocumentActivity::class.java)
            intent.putExtra("ActivityType", "Creation")
            intent.putExtra("userId", pUserId)
            startActivity(intent)
        }

        logout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent =Intent(this@MainActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        personalList.setOnClickListener {
            if(this.IsPersonalList)
            {
                this.IsPersonalList = false
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<MapFragment>(R.id.fragment_container)
                    pActiveFragment = FragmentTypes.MAP
                    ChangeFragment.isVisible = true
                    personalList.setImageResource(R.drawable.ic_person)
                }
            }
            else
            {
                this.IsPersonalList = true
                supportFragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<ListFragment>(R.id.fragment_container)
                    pActiveFragment = FragmentTypes.LIST
                    ChangeFragment.isVisible = false
                    personalList.setImageResource(R.drawable.ic_back)
                }
            }

        }

        if(savedInstanceState == null)
        {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<MapFragment>(R.id.fragment_container)
            }
        }
    }


    fun ShowDocumentOnMap(parDocument : Document)
    {
        this.IsPersonalList = false
        this.DocumentToShow = parDocument
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            replace<MapFragment>(R.id.fragment_container)
            pActiveFragment = FragmentTypes.MAP
            ChangeFragment.setImageResource(R.drawable.ic_list)
        }


    }

}
