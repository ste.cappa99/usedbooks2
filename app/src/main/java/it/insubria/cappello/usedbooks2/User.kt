package it.insubria.cappello.usedbooks2

import java.io.Serializable

data class User (var key : String, var name : String, var surname : String, var phoneNuber : String, var email : String ) : Serializable
{
    constructor(): this("", "", "", "", "")

    public fun getCompleteName() : String{
        return this.name + " " + this.surname
    }

}