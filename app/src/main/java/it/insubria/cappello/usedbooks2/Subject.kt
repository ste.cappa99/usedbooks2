package it.insubria.cappello.usedbooks2

data class Subject(private var id : String, var description : String) {

    constructor() : this("", "")

    public fun getId() : String{
        return id
    }

}