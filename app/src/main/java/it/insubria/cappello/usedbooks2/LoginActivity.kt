package it.insubria.cappello.usedbooks2

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        registerBtn.setOnClickListener{

            var intent : Intent = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

        signinBtn.setOnClickListener{
            when{
                TextUtils.isEmpty(emailLogin.text.toString().trim { it <= ' ' }) -> {
                    layout_email.setError(getString(R.string.insert_email_error))
                }

                TextUtils.isEmpty(passwordLogin.text.toString().trim { it <= ' ' }) -> {
                    layout_password.setError(getString(R.string.insert_password_error))
                }

                !this.pCheckEmail(emailLogin.text.toString()) ->{
                    layout_email.setError(getString(R.string.valid_mail_error))
                }

                !this.pCheckPassword(passwordLogin.text.toString()) -> {
                    layout_password.setError(getString(R.string.valid_password_error))
                }

                else -> {

                    val email: String = emailLogin.text.toString().trim { it <= ' ' }
                    val password: String = passwordLogin.text.toString().trim { it <= ' ' }

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(

                            OnCompleteListener<AuthResult> { task ->

                                if (task.isSuccessful) {

                                    var firebaseUser: FirebaseUser = task.result!!.user!!

                                    Toast.makeText(
                                        this@LoginActivity,
                                        getString(R.string.login_success),
                                        Toast.LENGTH_LONG
                                    ).show()


                                    val intent =
                                        Intent(this@LoginActivity, MainActivity::class.java)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    intent.putExtra("userId", firebaseUser.uid)
                                    intent.putExtra("email", email)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    Toast.makeText(
                                        this@LoginActivity,
                                        task.exception!!.message.toString(),
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            })
                }
            }
        }
    }

    private fun pCheckEmail(parEmail : String) : Boolean
    {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

        if(parEmail.matches(emailPattern.toRegex()))
            return true;
        else
            return false

    }

    private fun pCheckPassword(parPassword : String) : Boolean
    {
        val tPasswordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!\\-_?&])(?=\\S+$).{8,}"
        if(parPassword.matches(tPasswordPattern.toRegex()))
            return true
        else
            return false
    }


}