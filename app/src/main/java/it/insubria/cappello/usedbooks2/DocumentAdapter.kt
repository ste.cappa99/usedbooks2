package it.insubria.cappello.usedbooks2

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.view.isVisible

class DocumentAdapter(private val context: Context, private val data: MutableList<Document>)  : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var tNewView = convertView

        if (convertView == null)
            tNewView = LayoutInflater.from(context).inflate(R.layout.document_item, parent, false)

        if (tNewView != null) {
            val tTitle: TextView = tNewView.findViewById(R.id.title)
            val tDescription: TextView = tNewView.findViewById(R.id.description)
            val tPrice : TextView = tNewView.findViewById(R.id.price)
            val tCreationDate : TextView = tNewView.findViewById(R.id.creationDate)
            val tType : TextView = tNewView.findViewById(R.id.type)
            val tButton : ImageButton = tNewView.findViewById(R.id.modifyButton)
            tTitle.text = data[position].Subject
            tDescription.text = data[position].Description
            tPrice.text = data[position].Price.toString() + "€"
            tCreationDate.text = data[position].CreationDate
            tType.text = data[position].Type

            if(data[position].IsEditable)
            {
                tButton.setImageResource(R.drawable.ic_modify)

                tButton.setOnClickListener {
                    val tIntent = Intent(context, CreateDocumentActivity::class.java)
                    tIntent.putExtra("ActivityType", "Update")
                    tIntent.putExtra("Document", data[position])
                    tIntent.putExtra("userId", data[position].User)
                    context.startActivity(tIntent)
                }
            }
            else
            {
                tButton.setImageResource(R.drawable.ic_view_on_map)
                tButton.setOnClickListener {

                    (context as MainActivity).ShowDocumentOnMap(data[position])
                }

            }

        }

        return tNewView!!
    }

    override fun getItem(position: Int): Any {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }


}