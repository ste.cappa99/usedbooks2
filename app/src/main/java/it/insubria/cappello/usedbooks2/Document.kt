package it.insubria.cappello.usedbooks2

import java.io.Serializable
import java.util.*

data class Document
(
    var Type : String,
    var Subject : String,
    var Latitude : Double?,
    var Longitude : Double?,
    var User : String,
    var Price : Double,
    var Description : String,
    var CreationDate : String
) : Serializable
{

    private var key : String = ""
    var IsEditable = false

    constructor() : this("", "", null, null, "", 0.0, "", "")

    fun InizializeKey(parKey : String)
    {
        key = parKey
    }

    fun GetStringKey(): String{
        return key
    }

}