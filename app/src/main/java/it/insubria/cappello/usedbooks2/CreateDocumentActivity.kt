package it.insubria.cappello.usedbooks2

import android.content.Intent
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.core.view.isVisible
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_create.*
import java.lang.Exception
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class CreateDocumentActivity : AppCompatActivity() {

    private var pIsUpdate = false
    private lateinit var pDocument : Document
    private lateinit var pUserId : String
    private lateinit var pUser : User
    private var pDatabaseReference : DatabaseReference = FirebaseDatabase.getInstance().reference
    private val pSubjects : MutableList<Subject> = ArrayList()
    private lateinit var pAdapter: SubjectAdapter
    private val pSubjectEventListener : ChildEventListener = object : ChildEventListener{
        override fun onCancelled(error: DatabaseError) {}

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            val tModifiedSubject = snapshot.children.firstOrNull()

            if(tModifiedSubject != null)
            {
                this@CreateDocumentActivity.pSubjects.find { parSubject -> parSubject.getId() == snapshot.key!! }?.description = tModifiedSubject.value.toString()
            }

            this@CreateDocumentActivity.pAdapter.notifyDataSetChanged()

        }

        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            val tDescription = snapshot.children.firstOrNull()
            if( tDescription != null )
            {
                val tNewSubject = Subject(snapshot.key!!, tDescription.value.toString())
                this@CreateDocumentActivity.pSubjects.add(tNewSubject)
            }

            this@CreateDocumentActivity.pAdapter.notifyDataSetChanged()
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            val tModifiedSubject = snapshot.children.firstOrNull()

            if(tModifiedSubject != null)
            {
               val tRemoveSubject = this@CreateDocumentActivity.pSubjects.find { parSubject -> parSubject.getId() == snapshot.key!! }
               if(tRemoveSubject != null) {
                   pSubjects.remove(tRemoveSubject)
               }
            }

            this@CreateDocumentActivity.pAdapter.notifyDataSetChanged()


        }

    }

    private val pDocumentEventListener : ChildEventListener = object : ChildEventListener{
        override fun onCancelled(error: DatabaseError) {}

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            if(snapshot.key!! == pDocument.GetStringKey())
            {
                pDocument.Description = snapshot.child(getString(R.string.description)).value.toString()
                pDocument.Price = snapshot.child(getString(R.string.price)).value.toString().toDouble()
                pDocument.Subject = snapshot.child(getString(R.string.subject)).value.toString()
                pDocument.Type = snapshot.child(getString(R.string.type)).value.toString()
                pDocument.Latitude = snapshot.child(getString(R.string.latitude)).value.toString().toDouble()
                pDocument.Longitude = snapshot.child(getString(R.string.longitude)).value.toString().toDouble()
            }

            this@CreateDocumentActivity.pSetData()
        }

        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            if(snapshot.key!! == pDocument.GetStringKey()) {
                onBackPressed()
                finish()
            }
        }


    }

    private val pUserEventListener : ChildEventListener = object : ChildEventListener{
        override fun onCancelled(error: DatabaseError) {}

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            if(snapshot.key!! == pDocument.User)
            {
                pUser.name = snapshot.child(getString(R.string.name)).value.toString()
                pUser.surname = snapshot.child(getString(R.string.surname)).value.toString()
                pUser.phoneNuber = snapshot.child(getString(R.string.phone)).value.toString()
                pUser.email = snapshot.child(getString(R.string.email)).value.toString()
            }

            this@CreateDocumentActivity.pSetData()
        }

        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            if(snapshot.key!! == pDocument.User)
            {
                pUser.name = snapshot.child(getString(R.string.name)).value.toString()
                pUser.surname = snapshot.child(getString(R.string.surname)).value.toString()
                pUser.phoneNuber = snapshot.child(getString(R.string.phone)).value.toString()
                pUser.email = snapshot.child(getString(R.string.email)).value.toString()
            }

            this@CreateDocumentActivity.pSetData()
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            if(snapshot.key!! == pDocument.User) {
                onBackPressed()
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create)

        this.pUser = User()

        if(intent.hasExtra("ActivityType"))
        {
            when{
                intent.getStringExtra("ActivityType") == "Creation" ->{

                    DescriptionView.text = getString(R.string.create_a_new_document)
                    subjectSpn.isEnabled = true
                    descriptionTxt.isEnabled = true
                    addressTxt.isEnabled = true
                    zipCodeTxt.isEnabled = true
                    nationTxt.isEnabled = true
                    cityTxt.isEnabled = true
                    typeSpn.isEnabled = true
                    createDocument.isVisible = true
                    contactButtons.isVisible = false
                    linearInfo.isVisible = false

                    this.pIsUpdate = false

                }

                intent.getStringExtra("ActivityType") == "Details" ->{
                    this.pDocument = (intent.getSerializableExtra("Document")) as Document
                    DescriptionView.text = getString(R.string.docuemnt_details)
                    subjectSpn.isEnabled = false
                    descriptionTxt.isEnabled = false
                    priceTxt.isEnabled = false
                    typeSpn.isEnabled = false
                    positionLayout.isVisible = false
                    createDocument.isVisible = false
                    contactButtons.isVisible = true
                    linearInfo.isVisible = true

                    this.pIsUpdate = false
                }

                intent.getStringExtra("ActivityType") == "Update" -> {
                    this.pDocument = (intent.getSerializableExtra("Document")) as Document
                    DescriptionView.text = getString(R.string.document_update)
                    createDocument.text = getString(R.string.update_now)
                    subjectSpn.isEnabled = true
                    descriptionTxt.isEnabled = true
                    priceTxt.isEnabled = true
                    typeSpn.isEnabled = true
                    positionLayout.isVisible = true
                    createDocument.isVisible = true
                    contactButtons.isVisible = false
                    linearInfo.isVisible = false

                    this.pIsUpdate = true
                }
            }
        }

        if(intent.hasExtra("userId"))
        {
            this.pUserId = intent.getStringExtra("userId")
        }

        pAdapter = SubjectAdapter(this, pSubjects)

        back.setOnClickListener {
            onBackPressed()
        }

        createDocument.setOnClickListener {
            if(this.pCheckAllField())
            {
                val tDocument : Document
                val (tLat, tLong) = this.pGetPositionFromAddress("${addressTxt.text} ${cityTxt.text} ${zipCodeTxt.text} ${nationTxt.text}")
                val tSubject : String

                if((subjectSpn.selectedItem as Subject).description == "Other") {
                    tSubject = subjectTxt.text.toString()
                    if(!pSubjects.any { parSub -> parSub.description.toUpperCase() == tSubject.toUpperCase() })
                        pDatabaseReference.child(getString(R.string.subjects)).push().child(getString(R.string.description)).setValue(tSubject.toUpperCase())

                } else{
                    tSubject =  (subjectSpn.selectedItem as Subject).description
                }

                //tDocument = Document(DocumentTypes.Book.name, tSubject, tLat, tLong, this.pUserId, priceTxt.text.toString().toDouble(), descriptionTxt.text.toString(), Date().toString())

                if(tLat != null && tLong != null)
                {
                    var tMap = HashMap<String, String>()
                    tMap.put(getString(R.string.type), typeSpn.selectedItem.toString())
                    tMap.put(getString(R.string.subject), tSubject)
                    tMap.put(getString(R.string.latitude), tLat.toString())
                    tMap.put(getString(R.string.longitude), tLong.toString())
                    tMap.put(getString(R.string.user), this.pUserId)
                    tMap.put(getString(R.string.price), priceTxt.text.toString())
                    tMap.put(getString(R.string.description), descriptionTxt.text.toString())
                    tMap.put(getString(R.string.creationDate), Date().toString())

                    if(this.pIsUpdate)
                    {
                        pDatabaseReference.child(getString(R.string.documents)).child(this.pDocument.GetStringKey()).updateChildren(
                            tMap as Map<String, Any>
                        )
                    }
                    else
                    {
                        pDatabaseReference.child(getString(R.string.documents)).push().setValue(tMap)
                    }

                    onBackPressed()
                    finish()
                }
                else
                {
                    Toast.makeText(this@CreateDocumentActivity, getString(R.string.check_email), Toast.LENGTH_LONG).show()
                }

            }
        }

        emailContact.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(Intent.EXTRA_EMAIL, pUser.email)
                putExtra(Intent.EXTRA_SUBJECT, getString(R.string.used_book_contact))
                putExtra(Intent.EXTRA_TEXT, getString(R.string.body_contact))
            }
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        }

        smsContact.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO).apply {
                type = "text/plain"
                data = Uri.parse("smsto:${pUser.phoneNuber}")
                putExtra("sms_body", getString(R.string.body_contact))
            }
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        }

        subjectSpn.adapter = this.pAdapter

        typeSpn.adapter = ArrayAdapter<String>(this, R.layout.spinner_item, arrayOf(DocumentTypes.Book.name, DocumentTypes.Notes.name))

        subjectSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if(pAdapter.getItem(position).description == "Other")
                {
                    subjectTxt.isEnabled = true
                    subjectTxt.isVisible = true
                }
                else
                {
                    subjectTxt.isEnabled = false
                    subjectTxt.isVisible = false
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        if(intent.getStringExtra("ActivityType") != "Creation")
        {
            this.pDatabaseReference.child(getString(R.string.users)).addChildEventListener(this.pUserEventListener)
            this.pSetData()
        }
        else
        {
            this.pDatabaseReference.child(getString(R.string.subjects)).addChildEventListener(this.pSubjectEventListener)
        }


        this.pDatabaseReference.child(getString(R.string.documents)).addChildEventListener(this.pDocumentEventListener)

    }

    override fun onStop() {
        super.onStop()

        this.pDatabaseReference.removeEventListener(this.pSubjectEventListener)
        this.pDatabaseReference.removeEventListener(this.pDocumentEventListener)
        this.pDatabaseReference.removeEventListener(this.pUserEventListener)
    }


    private fun pCheckAllField() : Boolean
    {
        var tOk = true;

        if(TextUtils.isEmpty(addressTxt.text.trim()))
        {
            addressTxt.setError(getString(R.string.insert_address_error))
            tOk = false;
        }

        if(TextUtils.isEmpty(zipCodeTxt.text.trim()))
        {
            zipCodeTxt.setError(getString(R.string.insert_zipcode_error))
            tOk = false;
        }

        if(TextUtils.isEmpty(nationTxt.text.trim()))
        {
            nationTxt.setError(getString(R.string.insert_nation_error))
            tOk = false;
        }

        if(TextUtils.isEmpty(cityTxt.text.trim()))
        {
            cityTxt.setError(getString(R.string.insert_city_error))
            tOk = false;
        }

        if(TextUtils.isEmpty(priceTxt.text.trim()))
        {
            priceTxt.setError(getString(R.string.insert_price_error))
            tOk = false;
        }

        if((subjectSpn.selectedItem as Subject).description == "Other" && TextUtils.isEmpty(subjectTxt.text.trim()))
        {
            subjectTxt.setError(getString(R.string.insert_subject_error))
            tOk = false;
        }

        if(zipCodeTxt.text.trim().length < 5)
        {
            zipCodeTxt.setError(getString(R.string.invalid_zip_error))
            tOk = false;
        }

        return tOk;
    }

    private fun pGetPositionFromAddress(parAddress : String) : Pair<Double?, Double?>
    {
        val tGeocoder : Geocoder = Geocoder(this)
        try {
            val tAddress = tGeocoder.getFromLocationName(parAddress, 5)
            if(tAddress == null)
                return Pair(null, null)

            val tLocation : Address = tAddress.get(0)

            return Pair(tLocation.latitude, tLocation.longitude)
        }catch(exception : Exception) {

            Toast.makeText(this, "${exception.message}", Toast.LENGTH_LONG).show()

            return Pair(null, null)
        }
    }

    private fun pGetPositionFromLatLong(parLat : Double, parLong : Double) : String
    {
        val tGeocoder : Geocoder = Geocoder(this)
        try {
            val tAddress = tGeocoder.getFromLocation(parLat, parLong, 5)
            if(tAddress == null)
                return ""

            val tLocation : Address = tAddress.get(0)

            return tLocation.getAddressLine(0)
        }catch(exception : Exception) {

            Toast.makeText(this, "${exception.message}", Toast.LENGTH_LONG).show()

            return ""
        }
    }


    private fun pSetData(){

        if(this.pDocument.Type == DocumentTypes.Book.name)
            typeSpn.setSelection(0)
        else
            typeSpn.setSelection(1)
        pSubjects.clear()
        pSubjects.add(Subject("0", this.pDocument.Subject ))
        this.pAdapter.notifyDataSetChanged()
        val tPosition = this.pGetPositionFromLatLong(this.pDocument.Latitude!!, this.pDocument.Longitude!!)
        subjectSpn.setSelection(0)
        descriptionTxt.setText(this.pDocument.Description)
        priceTxt.setText(this.pDocument.Price.toString())
        userInfo.setText("User: ${this.pUser.getCompleteName()} \nPhone Number: ${this.pUser.phoneNuber}, \t Email: ${pUser.email} \nPosition: ${tPosition}" )
    }
}