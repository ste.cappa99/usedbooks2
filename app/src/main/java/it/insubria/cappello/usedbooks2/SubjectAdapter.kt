package it.insubria.cappello.usedbooks2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView

class SubjectAdapter(private val context: Context, private val data: MutableList<Subject>) : BaseAdapter(){

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var tNewView = convertView

        if (convertView == null)
            tNewView = LayoutInflater.from(context).inflate(R.layout.spinner_item, parent, false)

        if (tNewView != null) {
            val tDescription: TextView = tNewView.findViewById(R.id.descriptionTxt)
            tDescription.text = data[position].description
        }

        return tNewView!!
    }

    override fun getItem(position: Int): Subject {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }


}