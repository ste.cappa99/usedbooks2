package it.insubria.cappello.usedbooks2

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.map_fragment.*

class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var pMoveMap = true
    private var pDatabaseReference : DatabaseReference = FirebaseDatabase.getInstance().reference
    private var pDocuments : MutableList<Document> = ArrayList()
    private val pDocumentEventListener : ChildEventListener = object : ChildEventListener {
        override fun onCancelled(error: DatabaseError) {}

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            val tModifiedDocument = this@MapFragment.pDocuments.find { parDoc -> parDoc.GetStringKey() == snapshot.key!! }
            if(tModifiedDocument != null) {
                tModifiedDocument.Type = snapshot.child(getString(R.string.type)).value.toString()
                tModifiedDocument.Subject = snapshot.child(getString(R.string.subject)).value.toString()
                tModifiedDocument.Latitude = snapshot.child(getString(R.string.latitude)).value.toString().toDouble()
                tModifiedDocument.Longitude = snapshot.child(getString(R.string.longitude)).value.toString().toDouble()
                tModifiedDocument.Price = snapshot.child(getString(R.string.price)).value.toString().toDouble()
                tModifiedDocument.Description = snapshot.child(getString(R.string.description)).value.toString()
                tModifiedDocument.CreationDate = snapshot.child(getString(R.string.creationDate)).value.toString()

            }

            this@MapFragment.pUpdateMarkers()
        }

        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            val tType = snapshot.child(getString(R.string.type)).value.toString()
            val tSubject = snapshot.child(getString(R.string.subject)).value.toString()
            val tLatitude = snapshot.child(getString(R.string.latitude)).value.toString().toDouble()
            val tLongitude = snapshot.child(getString(R.string.longitude)).value.toString().toDouble()
            val tPrice = snapshot.child(getString(R.string.price)).value.toString().toDouble()
            val tDescription = snapshot.child(getString(R.string.description)).value.toString()
            val tCreationDate = snapshot.child(getString(R.string.creationDate)).value.toString()
            val tUserId = snapshot.child(getString(R.string.user)).value.toString()

            val tNewDocument = Document(tType,tSubject,tLatitude,tLongitude,tUserId,tPrice,tDescription,tCreationDate)
            tNewDocument.InizializeKey(snapshot.key!!)
            if(!(this@MapFragment.activity as MainActivity).IsPersonalList)
            {
                if(tNewDocument.User != (this@MapFragment.activity as MainActivity).pUserId)
                {
                    this@MapFragment.pDocuments.add(tNewDocument)
                    this@MapFragment.pUpdateMarkers()
                }
            }


        }

        override fun onChildRemoved(snapshot: DataSnapshot) {

            val tRemovedDocument = this@MapFragment.pDocuments.find { parDoc -> parDoc.GetStringKey() == snapshot.key!! }
            if(tRemovedDocument != null) {
                pDocuments.remove(tRemovedDocument)
            }

            this@MapFragment.pUpdateMarkers()
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater!!.inflate(R.layout.map_fragment, container, false)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return view
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter{
            override fun getInfoContents(p0: Marker): View? {
                val tView : View = layoutInflater.inflate(R.layout.activity_map_win, null)
                val tTitle = tView.findViewById<TextView>(R.id.infoTitle)
                val tDescription = tView.findViewById<TextView>(R.id.infoDescription)

                tTitle.text = p0.title
                tDescription.text = p0.snippet

                return tView
            }

            override fun getInfoWindow(p0: Marker): View? {
                return null
            }


        })
    }

    override fun onStart() {
        super.onStart()
        this.pDatabaseReference.child(getString(R.string.documents)).addChildEventListener(this.pDocumentEventListener)
    }

    override fun onStop() {
        super.onStop()
        this.pDatabaseReference.removeEventListener(this.pDocumentEventListener)
    }

    private fun pUpdateMarkers(){
        mMap.clear()

        for(tDoc in this.pDocuments)
        {
            if(tDoc.Latitude != null && tDoc.Longitude != null)
            {
                val tMarker = LatLng(tDoc.Latitude!!, tDoc.Longitude!!)
                mMap.addMarker(MarkerOptions().position(tMarker).title(tDoc.Subject).snippet("${tDoc.Description}" ))

                if((this.activity as MainActivity).DocumentToShow != null)
                {
                    if((this.activity as MainActivity).DocumentToShow!!.GetStringKey() == tDoc.GetStringKey())
                    {
                        this.pMoveCameraOnMarker(tMarker)
                        this.pMoveMap = false;
                        (this.activity as MainActivity).DocumentToShow = null
                    }
                }
                else
                {
                    if(this.pMoveMap)
                    {
                        this.pMoveCameraOnMarker(tMarker)
                        this.pMoveMap = false;
                    }
                }
            }

        }

    }

    private fun pMoveCameraOnMarker(parMarker : LatLng)
    {
        this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parMarker, 15.0f))
    }

}