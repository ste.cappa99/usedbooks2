package it.insubria.cappello.usedbooks2

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private var pDatabaseReference : DatabaseReference = FirebaseDatabase.getInstance().reference
    private lateinit var pUserId : String
    private var pUpdateUser = false
    private var pUser : User = User()

    private val pUserEventListener : ChildEventListener = object : ChildEventListener {
        override fun onCancelled(error: DatabaseError) {}

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            if(snapshot.key!! == pUserId)
            {
                pUser.name = snapshot.child(getString(R.string.name)).value.toString()
                pUser.surname = snapshot.child(getString(R.string.surname)).value.toString()
                pUser.phoneNuber = snapshot.child(getString(R.string.phone)).value.toString()
                pUser.email = snapshot.child(getString(R.string.email)).value.toString()
            }

            this@RegisterActivity.pUpdateData()
        }

        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            if(snapshot.key!! == pUserId)
            {
                pUser.name = snapshot.child(getString(R.string.name)).value.toString()
                pUser.surname = snapshot.child(getString(R.string.surname)).value.toString()
                pUser.phoneNuber = snapshot.child(getString(R.string.phone)).value.toString()
                pUser.email = snapshot.child(getString(R.string.email)).value.toString()
            }

            this@RegisterActivity.pUpdateData()
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            if(snapshot.key!! == pUserId) {
                onBackPressed()
                finish()
            }
        }
    }

    override fun onStart() {
        super.onStart()

        if(this.pUpdateUser)
            this.pDatabaseReference.child(getString(R.string.users)).addChildEventListener(this.pUserEventListener)
    }

    override fun onStop() {
        super.onStop()

        if(this.pUpdateUser)
            this.pDatabaseReference.removeEventListener(this.pUserEventListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        cancelButton.setOnClickListener{
            onBackPressed()
        }

        if(intent.hasExtra("userId"))
        {
            this.pUserId = intent.getStringExtra("userId")
            this.pUpdateUser = true
            layout_password.isVisible = false
            passwordInfo.isVisible = false
            title_register.text = getString(R.string.update_user)
            registerButton.text = getString(R.string.update_user_info)

        }
        else
        {
            this.pUpdateUser = false
            layout_password.isVisible = true
            passwordInfo.isVisible = true
            title_register.text = getString(R.string.registration)
            registerButton.text = getString(R.string.register_now)
        }

        registerButton.setOnClickListener{
            if(this.pCheckAllValues()){
                    val email: String = emailTxt.text.toString().trim { it <= ' ' }
                    val name : String = nameTxt.text.toString().trim { it <= ' ' }
                    val surname : String = surnameTxt.text.toString().trim { it <= ' ' }
                    val phone : String = phoneTxt.text.toString().trim { it <= ' ' }

                if(this.pUpdateUser)
                {
                    pDatabaseReference.child(getString(R.string.users)).child(this.pUserId).child(getString(R.string.name)).setValue(name)
                    pDatabaseReference.child(getString(R.string.users)).child(this.pUserId).child(getString(R.string.surname)).setValue(surname)
                    pDatabaseReference.child(getString(R.string.users)).child(this.pUserId).child(getString(R.string.phone)).setValue(phone)
                    pDatabaseReference.child(getString(R.string.users)).child(this.pUserId).child(getString(R.string.email)).setValue(email)
                    onBackPressed()
                    finish()
                }
                else
                {
                    val password: String = passwordTxt.text.toString().trim { it <= ' ' }

                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(

                            OnCompleteListener<AuthResult> { task ->

                                if (task.isSuccessful) {

                                    var firebaseUser: FirebaseUser = task.result!!.user!!

                                    Toast.makeText(
                                        this@RegisterActivity,
                                        getString(R.string.registere_success),
                                        Toast.LENGTH_LONG
                                    ).show()

                                    pDatabaseReference.child(getString(R.string.users)).child(firebaseUser.uid).child(getString(R.string.name)).setValue(name)
                                    pDatabaseReference.child(getString(R.string.users)).child(firebaseUser.uid).child(getString(R.string.surname)).setValue(surname)
                                    pDatabaseReference.child(getString(R.string.users)).child(firebaseUser.uid).child(getString(R.string.phone)).setValue(phone)
                                    pDatabaseReference.child(getString(R.string.users)).child(firebaseUser.uid).child(getString(R.string.email)).setValue(email)

                                    val intent =
                                        Intent(this@RegisterActivity, MainActivity::class.java)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    intent.putExtra("userId", firebaseUser.uid)
                                    intent.putExtra("email", email)

                                    startActivity(intent)
                                    finish()
                                } else {
                                    Toast.makeText(
                                        this@RegisterActivity,
                                        task.exception!!.message.toString(),
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            })
                }


            }
        }
    }


    private fun pCheckEmail(parEmail : String) : Boolean
    {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

        if(parEmail.matches(emailPattern.toRegex()))
            return true;
        else
            return false

    }

    private fun pCheckPassword(parPassword : String) : Boolean
    {
        val tPasswordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!\\-_?&])(?=\\S+$).{8,}"
        if(parPassword.matches(tPasswordPattern.toRegex()))
            return true
        else
            return false
    }

    private fun pCheckAllValues() : Boolean{

        var tOk = true

        if(TextUtils.isEmpty(emailTxt.text.toString().trim { it <= ' ' })) {
            layout_email.setError(getString(R.string.insert_email_error))

            tOk = false
        }

        if(TextUtils.isEmpty(nameTxt.text.toString().trim { it <= ' ' })) {
            layout_name.setError(getString(R.string.insert_name_error))
            tOk = false
        }

        if(TextUtils.isEmpty(surnameTxt.text.toString().trim { it <= ' ' })){
            layout_surname.setError(getString(R.string.insert_surname_error))
            tOk = false
        }

        if(TextUtils.isEmpty(phoneTxt.text.toString().trim { it <= ' ' })){
            layout_phone.setError(getString(R.string.insert_phone_error))
            tOk = false
        }

        if(!this.pCheckEmail(emailTxt.text.toString())){

            layout_email.setError(getString(R.string.valid_mail_error))
            tOk = false
        }

        if(!this.pUpdateUser)
        {
            if(!this.pCheckPassword(passwordTxt.text.toString())){
                layout_password.setError(getString(R.string.valid_password_error))
                tOk = false
            }

            if(TextUtils.isEmpty(passwordTxt.text.toString().trim { it <= ' ' })) {
                layout_password.setError(getString(R.string.insert_password_error))
                tOk = false
            }
        }

        return tOk
    }

    private fun pUpdateData()
    {
        nameTxt.setText(this.pUser.name)
        surnameTxt.setText(this.pUser.surname)
        emailTxt.setText(this.pUser.email)
        phoneTxt.setText(this.pUser.phoneNuber)
    }

}