package it.insubria.cappello.usedbooks2

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageButton
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.database.*

class ListFragment : Fragment() {

    private var pDatabaseReference : DatabaseReference = FirebaseDatabase.getInstance().reference
    private var pDocuments : MutableList<Document> = ArrayList()
    private lateinit var pDocumentAdapter : DocumentAdapter
    private val pDocumentEventListener : ChildEventListener = object : ChildEventListener {
        override fun onCancelled(error: DatabaseError) {}

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            val tModifiedDocument = this@ListFragment.pDocuments.find { parDoc -> parDoc.GetStringKey() == snapshot.key!! }
            if(tModifiedDocument != null) {
                tModifiedDocument.Type = snapshot.child(getString(R.string.type)).value.toString()
                tModifiedDocument.Subject = snapshot.child(getString(R.string.subject)).value.toString()
                tModifiedDocument.Latitude = snapshot.child(getString(R.string.latitude)).value.toString().toDouble()
                tModifiedDocument.Longitude = snapshot.child(getString(R.string.longitude)).value.toString().toDouble()
                tModifiedDocument.Price = snapshot.child(getString(R.string.price)).value.toString().toDouble()
                tModifiedDocument.Description = snapshot.child(getString(R.string.description)).value.toString()
                tModifiedDocument.CreationDate = snapshot.child(getString(R.string.creationDate)).value.toString()
                tModifiedDocument.User = snapshot.child(getString(R.string.user)).value.toString()
            }

            this@ListFragment.pDocumentAdapter.notifyDataSetChanged()
        }

        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            val tType = snapshot.child(getString(R.string.type)).value.toString()
            val tSubject = snapshot.child(getString(R.string.subject)).value.toString()
            val tLatitude = snapshot.child(getString(R.string.latitude)).value.toString().toDouble()
            val tLongitude = snapshot.child(getString(R.string.longitude)).value.toString().toDouble()
            val tPrice = snapshot.child(getString(R.string.price)).value.toString().toDouble()
            val tDescription = snapshot.child(getString(R.string.description)).value.toString()
            val tCreationDate = snapshot.child(getString(R.string.creationDate)).value.toString()
            val tUserId = snapshot.child(getString(R.string.user)).value.toString()

            val tNewDocument = Document(tType,tSubject,tLatitude,tLongitude,tUserId,tPrice,tDescription,tCreationDate)
            tNewDocument.InizializeKey(snapshot.key!!)

            if((this@ListFragment.activity as MainActivity).IsPersonalList)
            {
                if(tNewDocument.User == (this@ListFragment.activity as MainActivity).pUserId)
                {
                    tNewDocument.IsEditable = true
                    this@ListFragment.pDocuments.add(tNewDocument)
                    this@ListFragment.pDocumentAdapter.notifyDataSetChanged()
                }
            }
            else
            {
                if(tNewDocument.User != (this@ListFragment.activity as MainActivity).pUserId)
                {
                    tNewDocument.IsEditable = false
                    this@ListFragment.pDocuments.add(tNewDocument)
                    this@ListFragment.pDocumentAdapter.notifyDataSetChanged()
                }
            }
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {

            val tRemovedDocument = this@ListFragment.pDocuments.find { parDoc -> parDoc.GetStringKey() == snapshot.key!! }
            if(tRemovedDocument != null) {
                pDocuments.remove(tRemovedDocument)
            }

            this@ListFragment.pDocumentAdapter.notifyDataSetChanged()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var tView = inflater!!.inflate(R.layout.list_fragment, container, false)
        return tView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var tListView = view.findViewById(R.id.DocumentList) as ListView

        tListView.setOnItemClickListener(object : AdapterView.OnItemClickListener{
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                val intent = Intent(this@ListFragment.requireContext(), CreateDocumentActivity::class.java)
                intent.putExtra("ActivityType", "Details")
                intent.putExtra("Document", pDocuments[position])
                startActivity(intent)
            }
        })

        pDocumentAdapter = DocumentAdapter(this.requireContext(), pDocuments)
        tListView.adapter = pDocumentAdapter
    }

    override fun onStart() {
        super.onStart()

        this.pDocuments.clear()

        this.pDatabaseReference.child(getString(R.string.documents)).addChildEventListener(this.pDocumentEventListener)

    }

    override fun onStop() {
        super.onStop()
        this.pDatabaseReference.removeEventListener(this.pDocumentEventListener)
    }

}